#include "Utility.h"
/**
   This function returns true if two given parameters differ more than 1 percent.
*/
int Utility::diff(double a, double b)
{
  int A = (int) (a * 100);
  int B = (int) (b * 100);
  while (A > 100 || B > 100)
  {
    A /= 10;
    B /= 10;
  }
  int diff =  A - B;
  if (diff < 0)
  {
    diff = - diff;
  }
  //  if (diff > 1 || diff < -1)
  //    return true;
  return diff;
}
/**
   This function returns true if two given parameters differ more than d percent. precision: one tenth. 0.1
*/
bool Utility::diff(double a, double b, double d)
{
  int A = (int) (a * 1000);
  int B = (int) (b * 1000);
  while (A > 1000 || B > 1000)
  {
    A /= 10;
    B /= 10;
  }
  int diff =  A - B;
  if (diff > d * 10 || diff < - d * 10)
    return true;
  return false;
}
/**
   If the second parameter is zero, return -1.
*/
double Utility::ratio(double a, double b)
{
  double ratio;
  if (b == 0)
  {
    return -1;
  }
  else
  {
    ratio = (a / b);
    if (ratio > 1)
      ratio = 1 / ratio;
    ratio = 1 - ratio; // the higher the difference between recent, and previous power output, the colser should 'width' be to 1.0
  }
  return ratio;
}
void Utility::printVoltage()
{
#ifdef PERFORMANCE_TEST
  Serial.println(String(millis() / 1000.0) + ", " + String(Config::getVoltage()));
#endif
}
void Utility::report(String s) {
#ifndef SILENT
  Serial.println(s);
#endif
}
String Utility::stripNonNums(char * s) {
  int i = 0;
  String res = "";
  while (s[i] != '\0') {
    if ((s[i] <= '9' && s[i] >= '0') || s[i] == '-') {
      res += s[i];
    }
    i++;
  }
  return res;
}
bool Utility::is(char * s1, char * s2) {
  int i = 0;
  while (s1[i] == s2[i]) {
    if (s1[i] == '\0') {
      return true;
    }
    i++;
  }
  return false;
}
bool Utility::beginsWith(char * w, char * s, int len) {
  for (int i = 0; i < len; i++) {
    if (s[i] != w[i]) {
      return false;
    }
  }
  return true;
}
