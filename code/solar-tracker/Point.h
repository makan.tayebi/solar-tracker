#ifndef POINT_H
#define POINT_H
#include <Arduino.h>
#include "Utility.h"
class Point {
  private:
    int h, v;
    double voltage;
  public:
    Point();
    Point(int, int);
    int getHorizontal();
    int getVertical();
    void setHorizontal(int);
    void setVertical(int);
    void setDir(int, int);
    void setVolt(double);
    double getVolt();
    void printOut();
};
#endif
