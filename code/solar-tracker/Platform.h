#ifndef PLATFORM_H
#define PLATFORM_H
//#include <Arduino.h>
#include <Adafruit_PWMServoDriver.h>
#include "Point.h"
class Platform {
  private:
    Adafruit_PWMServoDriver pwm;
    Point * point;
    Point * searchFivePoints(double distance, Point * hub);
    Point * searchNinePoints(double distance);
    // SERVO_MIN  150
    // SERVO_MAX  600
  public:
    Platform();
    void setHorizontal(int);
    void setVertical(int);
    int getVertical();
    int getHorizontal();
    void left(int);
    void right(int);
    void up(int);
    void down(int);
    void moveTo(Point *);
    Point * search(double, double);
    enum Direction { Up, Down, Left, Right};
};
#endif
