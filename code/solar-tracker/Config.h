/* select the input pin for sensing the Voltage. it will map input voltages between 0 and 5 volts into integer values between 0 and 1023*/
#ifndef CONFIG_H
#define CONFIG_H
#define DEVELOPMENT
#define PERFORMANCE_TEST
//#define SILENT
class Config {
  private:
    //	static double SERIES_RESISTOR,PARALLEL_RESISTOR;
    static double currentVoltage;
  public:
    static void calculateVoltage();
    static int sensorPinLow, sensorPinHigh;
    static double getVoltage();
    static double getPower();
    static double getResistance();
};
#endif
