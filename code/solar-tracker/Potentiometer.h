#ifndef POTENTIOMETER_H
#define POTENTIOMETER_H
#include <Arduino.h>

class Potentiometer {
  private:
    static int resis;
  public:
    Potentiometer();
    static void setValue(uint8_t); // receivess a value between 0 and 255, representing zero to 5KOhm resistance.
    static int getValue();
    static double sweep();
    static double optimizeHillClimb();
};
#endif
