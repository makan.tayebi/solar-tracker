#include "Potentiometer.h"
#include "Config.h"
#include <Arduino.h>

/*
   The value of the voltage on the two ends of our load,
   calculated by the calculateVoltage function, is saved in this variable.
*/
double Config::currentVoltage;
/*
   This function measures the voltage a lot of times really fast, and returns the average value.
   That is one way to compensate the high inaccuracy of our measurement.
*/
void Config::calculateVoltage() {
  long sum = 0;
  double sensorValue;
  for ( int i = 0; i < 1000; i++)
  {
    sum += analogRead(A0);
    //    Serial.print(".");
  }
  //  Serial.println();
  sensorValue = sum / 1000.0;
  //  Serial.println("measured value:" + String (sensorValue));
  //  Serial.println("sum of 1000 measures: " + String (sum));
  currentVoltage = (sensorValue * 5.0 / 1024.0) * 3; // because the A0 pin of the arduino measures a third of the voltage.
}
/*
   This function does the job of returning the previously calculated Voltage.
*/
double Config::getVoltage() {
  return currentVoltage;
}
/*
   This function gets the previously calculated Voltage, gets the resistance, and calculates the Power.
*/
double Config::getPower()
{
  // P = V*I = V^2/R
  double v = getVoltage();
  return v * v / getResistance();
}
/*
   This function calculates the resistance, the way that is configured on the device,
   and according to the value of the potentiometer, if it is involved in the current setting, and returns it.
*/
double Config::getResistance()
{
  return (Potentiometer::getValue() * 40.0) / ((double) Potentiometer::getValue() + 40.0);
}