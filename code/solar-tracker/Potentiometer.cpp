#include "Potentiometer.h"
#include "Config.h"
#include "Utility.h"
#include <arduino.h>
#include <SPI.h>
int Potentiometer::resis;
Potentiometer::Potentiometer() {
  resis = 10;
  pinMode(9 , OUTPUT); // for SPI SS comm
  pinMode(10, OUTPUT); // for SPI SS comm
  pinMode(11, OUTPUT); // for SPI MOSI
  pinMode(13, OUTPUT); // for SPI CLK
  digitalWrite(10, HIGH);
}
void Potentiometer::setValue(uint8_t r) {
  if (r < 1)
    r = 1;
  if (r > 255)
    r = 255;
  SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
  digitalWrite(10, LOW);
  SPI.transfer(r);
  //Serial.print("Writing to first Pot...  " + String(r) + "\n");
  digitalWrite(10, HIGH);
  SPI.endTransaction();
  //  SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
  //  digitalWrite(9, LOW);
  //  SPI.transfer(r);
  //  Serial.print("Writing to second Pot... " + String(r) + "\n");
  //  digitalWrite(9, HIGH);
  //  SPI.endTransaction();
  resis = r;
}

int Potentiometer::getValue() {
  return 74.66 + (resis * 20.3359);
}
/**
    Walks the spectrum in one direction until it won't improve. Then changes direction
   and walks to the other direction until it won't improve again. With the second halt
   in improvement, we can for sure say that we have been on both sides of the peak, and
   we have climbed the hill in the middle for sure.
*/
double Potentiometer::optimizeHillClimb() {
  int uTurns = 0, increment = 1, bestI = resis;
  int initialResistance = resis;
  double prevPower = -1, bestPower = - 1;
  do {
    setValue(resis + increment); // value of resis will be changed.
    Config::calculateVoltage();
    //    Utility::report("setting value to Potentiometer position: " + String(resis) + " - " + String(Config::getResistance()) + " Ohms - " + String(Config::getPower()) + " W");
    if (bestPower < Config::getPower()) {
      bestPower = Config::getPower();
      bestI = resis;
    }
    if ( (Utility::diff(bestPower, Config::getPower(), 2) && bestPower > Config::getPower()) || resis == 1 || resis == 255 ) { // if the new power is less than bestPower, with a 5% margin, ...
      increment = - increment;
      uTurns++;
      setValue(bestPower);
    }
    prevPower = Config::getPower();
  } while (uTurns < 2);
  setValue(bestI);
  Utility::report(".. result: at Potentiometer position " + String(bestI) + "\t Maximum Power: " + String(bestPower) + " \tResistance total:" + String(Config::getResistance()));
  Config::calculateVoltage();
  return Config::getPower();
}
double Potentiometer::sweep() {
  double bestPower = 0;
  int bestI = 1;
  Utility::report("Sweeping through values... ");
  for (int i = 1; i < 100; i++)
  {
    Potentiometer::setValue(i);
    delay(1);
    Config::calculateVoltage();
    //    Serial.println("Potentiometer position " + String(i) + ".\t Power: " + String(Config::getPower())
    //      + " \t at voltage:" + String(Config::getVoltage()) + " \tResistance total:" + String(Config::getResistance())
    //      + "Ohm \t at Poten. Resistance: " + String(Potentiometer::getValue()) + "Ohm"
    //      );

    if (Config::getPower() > bestPower) {
      bestI = i;
      bestPower = Config::getPower();
    }
  }
  Potentiometer::setValue(bestI);
  Utility::report(".. result: at Potentiometer position " + String(bestI) + "\t Maximum Power: " + String(bestPower) + " \tResistance total:" + String(Config::getResistance()));
  return bestPower;
}
