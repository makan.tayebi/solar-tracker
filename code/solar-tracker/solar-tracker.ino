#include "Potentiometer.h"
#include "Config.h"
#include <Wire.h>
#include <SPI.h>
#include "Point.h"
#include "Platform.h"
#include "Utility.h"

///////////////////////////////////////SETUP&LOOP/////////////////////////////////////////////////
Platform * platform;
Potentiometer * pot;
char message[11];
int charCounter = 0;
bool optimize_on = false;
void setup() {

  //TODO: take this piece of code in the Potentiometer class.

  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  platform = new Platform();
  pot = new Potentiometer(); // everything is static, but we just need to initialize the SPI channel.
  message[10] = '\0';
  randomSeed(analogRead(0));
}
void loop() {  
  delay(1000);

  readCommands();
  Config::calculateVoltage();  
  Utility::printVoltage();
  if (optimize_on) {
    optimize();
  }
}
double prevVoltage = 0;
void optimizeSetup()
{
  Utility::report("Setting up Optimization");
  Utility::printVoltage();
  platform->moveTo(new Point(0, 0));
  Utility::printVoltage();
  platform->moveTo(platform->search(1, 0.90));
  Utility::report("Optimization Setup has found the best point and applied it.");
  Potentiometer::optimizeHillClimb();
  Utility::report("...done sweeping");
  Config::calculateVoltage();
  Utility::printVoltage();
  prevVoltage = Config::getVoltage();
}
void optimize()
{
  // measure the power output.
  //  Serial.println("V: " + String(Config::getVoltage()));
  // compare the previous measure output. if lower, search again.
  // TODO: go from monitoring voltage, to monitoring power. That's what this is about.
  if (Utility::diff(prevVoltage, Config::getVoltage(), 1)) // if there is two percent change. // more than two percent decrease in power output since last search. the 2 percent value is decided based on the measurement error and the precision of the Arduino.
  { // trigger search.
    Point * best = platform->search(Utility::ratio(prevVoltage, Config::getVoltage()), 0.50);
    //    Serial.println(" search's best point: " + String(best->getHorizontal()) + ":" + String(best->getVertical()));
    platform->moveTo(best);
    //    Serial.println("moved to best point");
    Utility::report("Optimizing potentiometer values... :");
    Potentiometer::optimizeHillClimb();
    Utility::report("...done sweeping");
    Config::calculateVoltage();
    prevVoltage = Config::getVoltage();
  }
}

void readCommands() {
  if (Serial.available() > 0) {
    // read the incoming byte:
    char incomingByte = Serial.read();
    if (incomingByte == '\n' || charCounter == 10) {
      message[charCounter] = '\0';
      charCounter = 0;
      // parse command
      if (Utility::beginsWith((char *) "stats", message, 3)) { // report
        Config::calculateVoltage();
        Serial.println(String(Config::getPower()) + "Watts ,\t\t" + String(Config::getVoltage()) + "V,\t\t" + String(Config::getResistance()) + " Ohms");
      } else if (Utility::beginsWith((char*) "roth", message, 4)) { // rotate
        int deg = Utility::stripNonNums(message).toInt();
        Utility::report("Rotate Horizontally " + String(deg) + " degrees.");
        platform->setHorizontal(deg);
      } else if (Utility::beginsWith((char*) "rotv", message, 4)) { // rotate
        int deg = Utility::stripNonNums(message).toInt();
        Utility::report("Rotate Verrically " + String(deg) + " degrees.");
        platform->setVertical(deg);
      } else if (Utility::beginsWith((char*) "res", message, 3)) { // set Potentiometer resistance
        int resis = Utility::stripNonNums(message).toInt();
        Potentiometer::setValue(resis);
        Utility::report("Potentiometer resistance: " + String(resis));
      } else if (Utility::is((char*) "track", message)) { // MPP & Solar Tracking
        Utility::report(" Tracking order received.");
        optimizeSetup();
        optimize_on = true;
      } else if (Utility::is((char*) "halt", message)) { // stop MPP & SOLAR Tracking
        Utility::report(" Halt order received.");
        optimize_on = false;
      } else {
        Utility::report("Message was not meaningfully parsed: " + String(message));
      }
    } else {
      message[charCounter] = incomingByte;
      charCounter++;
    }
  }
}
