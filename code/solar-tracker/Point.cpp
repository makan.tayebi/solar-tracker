#include "Point.h"
Point::Point()
{
}
Point::Point(int h, int v)
{
  setHorizontal(h);
  setVertical(v);
  voltage = -1;
}
void Point::setVolt(double volt)
{
  voltage = volt;
}
double Point::getVolt()
{
  return voltage;
}
void Point::printOut()
{
  Utility::report("H:" + String(h) + ", V:" + String(v));
}
void Point::setHorizontal(int h)
{
  if (h < -90 || h > 90)
    Serial.println("WARNING: Setting Horizontal value to " + String(h) + ".");
  this->h = h;
}
void Point::setVertical(int v)
{
  if (v < 0 || v > 90)
    Serial.println("WARNING: Setting Vertical value to " + String(v) + ".");
  this->v = v;
}
int Point::getHorizontal()
{
  return h;
}
int Point::getVertical()
{
  return v;
}
void Point::setDir(int h, int v)
{
  setHorizontal(h);
  setVertical(v);
}

