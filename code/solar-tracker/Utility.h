#ifndef UTILITY_H
#define UTILITY_H
#include <SPI.h>
#include "Config.h"
#include <Wire.h>
class Utility
{
  private:
  public:
    static void printVoltage();
    static int diff(double, double);
    static bool diff(double, double, double);
    static double ratio(double, double);
    static bool beginsWith(char * w, char * s, int len);
    static bool is(char * s1, char * s2);
    static String stripNonNums(char * s);
    static void report(String s);
};
#endif
