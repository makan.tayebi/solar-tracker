#include "Platform.h"
#include "Utility.h"
#include "Config.h"
#include "Potentiometer.h"
Platform::Platform() {
  point = new Point();
  pwm = Adafruit_PWMServoDriver();
  pwm.begin();
  pwm.setPWMFreq(60);  // Analog servos run at ~60 Hz updates
  yield();
}
/**
   Searching for the optimum point, until the best point found does not get better than the last iteration.
   @param width: the floating point number between 0.0 and 1.0, showing how wide should we search for the
   optimum point.
*/
Point * Platform::search(double width, double dampFactor)
{
  Utility::report("---search start---");
  Config::calculateVoltage();
  double prevVoltage = Config::getVoltage();
  Point * best = new Point(getHorizontal(), getVertical());
  Point * overallBestPoint = new Point(getHorizontal(), getVertical());
  //  int fruitlessHops = 0;
  double bestVoltage = 0;
  do {
    double angles = width * 140;
    if (angles < 2)
      angles = 2;
    Utility::report("Going to search with the final width of " + String(angles) + " degrees, centered around ");best->printOut();
    best = searchFivePoints(angles, best); // search a maximum of 50, min of 1 degree wide. Search around the previous best.
    Utility::report("New iteration's BEST is:");best->printOut();
    //    moveTo(best);
    //    Config::calculateVoltage();

    if (bestVoltage < best->getVolt()) //Config::getVoltage()
    {
      bestVoltage = best->getVolt(); //Config::getVoltage()
      overallBestPoint->setDir(best->getHorizontal(), best->getVertical());
      //      fruitlessHops = 0;
    }
    Utility::report("Best found point in current iteration: " + String(Config::getVoltage()) + " V, at point:"); best->printOut();
    width = dampFactor * Utility::diff(best->getVolt(), prevVoltage) / 100.0;
    Utility::report("Width of search as calculated for the next round:" + String(width));
    dampFactor *= dampFactor;
    prevVoltage = best->getVolt(); // save the power that was updated in the last iteration.
  } while (Utility::diff(width, 0.0, 1));
  Utility::report("---search done---"); Utility::report(" Best Voltage:" + String(bestVoltage)); Utility::report("Best point found: "); overallBestPoint->printOut();
  return overallBestPoint;
}
int Platform::getVertical()
{
  return point->getVertical();
}
int Platform::getHorizontal()
{
  return point->getHorizontal();
}
void Platform::up(int d) {
  setVertical(point->getVertical() + d);
}
void Platform::down(int d) {
  setVertical(point->getVertical() - d);
}
void Platform::left(int d) {
  setHorizontal(point->getHorizontal() + d);
}
void Platform::right(int d) {
  setHorizontal(point->getHorizontal() - d);
}
void Platform::setHorizontal(int d) {
  if (d > 90)
    d = 90;
  if (d < -90)
    d = -90;
  point->setHorizontal(d);
  d = (d * 2.555) + 70.0; // calibration: according to the linear model: a * HorizontalDeg + b = horizontalNum
  pwm.setPWM(0, 0, 325 + d); // 325 = (150 + 600) / 2
}
void Platform::setVertical(int d) {
  if (d > 90)
    d = 90;
  if (d < 0)
    d = 0;
  point->setVertical(d);
  d =  (2.368 - d) / 0.473; // calibration: according to the linear model: a * VerticalDeg + b = VerticalNum
  pwm.setPWM(1, 0, 325 + d); // 325 = (150 + 600) / 2
}
void Platform::moveTo(Point * p) {
  setVertical(p->getVertical());
  setHorizontal(p->getHorizontal());
}
/** This function searches 5 points around a square in the area around the initial point of the platform.
    Tests the power output of each of them, and returns the best direction among them.
    @param distance: must be in degrees.
    @param center: A point, containing the center of the area that should be searched.
*/
Point * Platform::searchFivePoints(double distance, Point * center)
{
  //  Point * initialDir = new Point(getHorizontal(), getVertical());
  Point * optimumDir = new Point(getHorizontal(), getVertical());
  optimumDir->setVolt(Config::getPower());
  for (int i = 0; i < 2; i++)
  {
    for (int j = 0; j < 2; j++)
    {
      // searching 4 places.
      setVertical (center->getVertical() + distance * i - distance / 2);
      setHorizontal (center->getHorizontal() + distance * j - distance / 2);
      delay(1000); // TODO: delay based on the degree of change.
      // measure.
      Config::calculateVoltage();
      Utility::printVoltage();
      if (optimumDir->getVolt() < Config::getPower()) // check if got better
      {
        optimumDir->setDir(getHorizontal(), getVertical());
        optimumDir->setVolt(Config::getPower());
      }
    }
  }
  //  platform->moveTo(initialDir);
  return optimumDir;
}
/** This function searches 9 points around a square in the area around the initial point of the platform.
    Tests the power output of each of them, and returns the best direction among them.
    @param distance: must be in degrees.
*/
Point * Platform::searchNinePoints(double distance) {
  Point * optimumDir = new Point();
  Point * initialDir = new Point(getHorizontal(), getVertical());
  double optimumPower = 0;

  for (int i = -1; i < 2; i++)
  {
    for (int j = -1; j < 2; j++)
    {
      // searching 9 places.
      setVertical (initialDir->getVertical() + i * distance);
      setHorizontal (initialDir->getHorizontal() + j * distance);
      delay(1000);
      // measure.
      Config::calculateVoltage();
      Utility::printVoltage();
      if (optimumPower < Config::getPower()) // check if got better
      {
        optimumPower = Config::getPower();
        optimumDir->setDir(getHorizontal(), getVertical());
      }
    }
  }
  return optimumDir;
}

